# P2 answer

## Merge Comments

Comment on the differences between your `p1_results` and the ones given for this practical.

In my code I misprinted the results, I printed the whole positional data through one variable.
In the code given, each position variable (x,y,xdot and ydot) was printed separatly.

## Result Comments

Are your results identical to the ones demanded in **practical 2**? If not, why?

When I run the final command >> ros2 topic pub /ref_position custom_messages/msg/RefPosition {"x: 0.0, y: 1.6, deltat: 2.0"} --once
The terminal gives me this error message : The passed message type is invalid
I have tried rebuilding, resourcing and checking the spelling, but nothing worked.
That's why I couldn't see results.

# P3 answer

## Merge Comments

The problem above mentionned was solved by adjusting the place of the brackets : 
ros2 topic pub /ref_position custom_messages/msg/RefPosition "{x: 0.0, y: 1.6, deltat: 2.0}" --once

Because the command formulations differ from one pc to another, and that's why it didn't work on mine with the brackets outside.

And in my code I didn't use a timer, it could have been better if I used it.

## Result Comments

The results I got where compatible with the correction, the method was a bit different.
In addition, the way I got to the path of the csv file was by writing the entire path by hand, because when I used the method to complete the current path it didn't work for me.

# P4 answer

## Merge Comments

## Result Comments

# P5 answer

## Merge Comments

For the simulator:

My code calculates the joint positions directly from the desired joint velocities upon receiving a callback, while the correction updates the joint positions periodically based on the desired joint velocities using a timer callback

For the controller:

In my code, I calculated the joints velocities right inside a specific part of the code. But we might run into unexpected problems because we're not checking for errors or ensuring that everything is up-to-date before we do the calculation. 
The corrected version uses a timer to regularly check and calculate the joint speeds.

But for both codes I can't see graphs being drawn in PlotJuggler I think the problem remains in my computer.

## Result Comments

Even though my code is mathematically correct but PlotJuggler didn't show any graphs.
(It might be a problem from my computer but I couldn't find a way to fix it).

# P6 answer

## Merge Comments

## Result Comments

The code works correctly, we can observe the robot arm moving (through the three positions its is ordered to move into) when we run the commands.
We can clearly see the effect of the disturbance through the rocking movement (because the disturbance is sinusoidal) of the arm while it is not ordered to move.
We had to add "export ROS_LOCALHOST_ONLY=1" to the .bashrc file to simulate the robot locally without affecting the simulations of others by publishing on the network. 
The RVIZ works correctly, we observe correctly the robot and its movements, but I couldn't find the problem that stops PlotJuggler from working properly.

