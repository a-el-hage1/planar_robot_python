# Practical 6

- Develop a `disturbance` node complying with the following characteristics streaming joint velocities disturbances in `/qdot_disturbance`, following an expressions such as $\dot{q}_{dist}=A\,\sin(\omega\,t)$, with $A$ and $\omega$ parameters of the node.

- Modify `simulator` node in order to consider these disturbances.

- Based on [this](https://docs.ros.org/en/foxy/Tutorials/Intermediate/Launch/Creating-Launch-Files.html) tutorial, write a launch file `planar_robot_python_launch.py` in a folder `launch` that initiates the nodes `trajectory_generator`, `controller`, `kinematic_model`, `simulator` and `disturbance`.

- The whole system should work properly when the following commands are performed

    - terminal 1: `ros2 launch planar_robot_python planar_robot_python_launch.py`
    - terminal 2: `rviz2 -d install/planar_robot_python/share/planar_robot_python/urdf/planar_robot.rviz`
    - terminal 3: `ros2 topic pub -1 /launch std_msgs/msg/Bool "{data: true}"`

In summary, the expected result when running `src/planar_robot_python/bash_scripts/p6.sh` should be the following:

![](../gif/p6.gif)