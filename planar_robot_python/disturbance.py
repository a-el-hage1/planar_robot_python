import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import numpy as np

class Disturbance(Node):
    def __init__(self):
        super().__init__('disturbance')

        self.declare_parameter('A', 1.5)
        self.declare_parameter('omega', 10)

        self.publisher_     = self.create_publisher(
            JointState, 
            'qdot_disturbance', 
            10) 

        self.qdot_disturbance             = np.zeros(2)
        self.sampling_period              = 1e-2
        self.timer                        = self.create_timer(self.sampling_period, self.timer_callback)   
        self.dt = 0.0  


    def timer_callback(self):

        #We will take the same quantity of disturbtion for both dimensions
        self.dt += self.sampling_period
        self.qdot_disturbance [0], self.qdot_disturbance[1] = self.get_parameter('A').value * np.sin((self.get_parameter('omega').value ) * self.dt),self.get_parameter('A').value * np.sin((self.get_parameter('omega').value ) * self.dt)

        joint_state                 = JointState()
        joint_state.velocity = self.qdot_disturbance
        self.publisher_.publish(joint_state)  
        

def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    disturbance.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
        
