import rclpy
from rclpy.node import Node
from custom_messages.msg import RefPosition, CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class TrajectoryGeneratorNode(Node):
    def __init__(self):
        super().__init__('trajectory_generator')

        self.declare_parameter('initial_pose', [0.0, 0.0])
        self.initial_pose = np.array(self.get_parameter('initial_pose').value)
        self.current_pose = self.initial_pose

        self.ref_position_subscription = self.create_subscription(
            RefPosition,
            'ref_position',
            self.ref_position_callback,
            10)

        self.desired_state_publisher = self.create_publisher(
            CartesianState,
            'desired_state',
            10)

        self.is_initial_pose_set = False

    def ref_position_callback(self, msg):
        if not self.is_initial_pose_set:
            self.current_pose = np.array([msg.x, msg.y])
            self.is_initial_pose_set = True

        position_interpolator = PositionInterpolation(
            self.current_pose, np.array([msg.x, msg.y]), msg.deltat)

        # Sample points along the trajectory and publish CartesianState messages
        num_points = int(msg.deltat / 0.01)  # Adjust the time step as needed
        for i in range(num_points + 1):
            t = i * msg.deltat / num_points
            position = position_interpolator.p(t)
            velocity = position_interpolator.pdot(t)

            cartesian_state_msg = CartesianState()
            cartesian_state_msg.x = position[0]
            cartesian_state_msg.y = position[1]
            cartesian_state_msg.xdot = velocity[0]
            cartesian_state_msg.ydot = velocity[1]

            self.desired_state_publisher.publish(cartesian_state_msg)

def main(args=None):
    rclpy.init(args=args)
    trajectory_generator_node = TrajectoryGeneratorNode()
    rclpy.spin(trajectory_generator_node)
    trajectory_generator_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()